## Service start order
To run the whole project correctly you need to:
1. Start [ws_tickers_gen](https://gitlab.com/ws_tickers_app/ws_tickers_gen)
2. Start [ws_tickers_back](https://gitlab.com/ws_tickers_app/ws_tickers_back)
3. Start [ws_tickers_front](https://gitlab.com/ws_tickers_app/ws_tickers_front)
---

## Project setup
You have two options to run this project. Using poetry and using docker.

### With poetry
```
poetry install
poetry run uvicorn main:app
```

### With docker
###### Run service:
```
sh run_service.sh
```
###### Stop service:
```
sh stop_service.sh
```