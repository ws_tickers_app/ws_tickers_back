from typing import Optional
from fastapi import FastAPI, WebSocket
from fastapi.middleware.cors import CORSMiddleware
import redis
import json
import asyncio


app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
redis_client = redis.StrictRedis(host="redis")


@app.get("/ticker/{item_id}/history")
def get_ticker_hist(item_id: Optional[str]):
    data = redis_client.lrange(item_id, 0, 2999)
    response = [json.loads(item) for item in data]
    response.reverse()
    return response


@app.get("/tickers")
def get_tickers():
    data = redis_client.keys()
    return data


@app.websocket("/ticker/{item_id}/stream")
async def websocket_endpoint(websocket: WebSocket, item_id: Optional[str]):
    await websocket.accept()
    while True:
        data = json.loads(redis_client.lrange(item_id, 0, 0)[0])
        await websocket.send_text(json.dumps(data))
        await asyncio.sleep(1)
